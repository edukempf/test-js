import http from 'http';

import app from './app';

export const listen = () => (
  new Promise((resolve, reject) => {

    const onError = error => {
      if (error.syscall !== 'listen') {
        throw error;
      }

      switch (error.code) {
        case 'EACCES':
          console.error(`3000 requires elevated privileges`);
          process.exit(1);
          break;
        case 'EADDRINUSE':
          console.error(`3000 is already in use`);
          process.exit(1);
          break;
        default:
          throw error;
      }
    };

    const onListening = () => {
      console.info(`Worker ${process.pid} started on port 3000`);
      resolve();
    };

    try {
      const server = http.createServer(app);
      server.on('error', onError);
      server.on('listening', onListening);
      server.listen(3000);
    } catch (ex) {
      reject(ex);
    }
  })

);

Promise.resolve()
    .then(() => listen())
    .then(() => {
        console.info('Ready!');
    })
    .catch(console.error);
