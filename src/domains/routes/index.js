import { join, resolve } from 'path';
import { readdirSync, statSync, existsSync } from 'fs';
import express from 'express';

const domainsDir = resolve(__dirname, '..');

const isDirectory = path => statSync(path).isDirectory();

const concat = (routes, dir) => {
    const path = `${dir}/routes`;
    if (!existsSync(`${path}.js`)) {
        return routes;
    }
    // eslint-disable-next-line
    const mod = require(path).default;
    return routes.concat(mod);
};

const load = (arr, app) => {
    arr.forEach(route => {
        const fn = app[route.method];
        fn.call(app, route.path, route.handler || route.handlers);
    });
    return app;
};

const routes = readdirSync(domainsDir)
    .map(file => join(domainsDir, file))
    .filter(isDirectory)
    .reduce(concat, []);

export default load(routes, express.Router());
