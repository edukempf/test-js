import controller from './controller';

export default [
    {
        method: 'get',
        path: '/estudos',
        handlers: [
            controller.responseOk,
        ],
    },
]