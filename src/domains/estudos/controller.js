const responseOk = (req, res, next) => {
    res.status(200).send({ message: "ok" });
};

export const isValidTest = valid => !valid;

export default {
    responseOk,
}