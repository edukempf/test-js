import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../src/app';

// Configure chai
chai.use(chaiHttp);
chai.should();

describe("Rota inicial", () => {
    describe("GET /estudos", () => {
        it("should have status 200", (done) => {
            chai.request(app)
                .get('/estudos')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });

        it("should have status 404", (done) => {
            chai.request(app)
                .get('/')
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                });
        });
    });
});
